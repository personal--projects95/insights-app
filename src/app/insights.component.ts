import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Insights, InsightsServiceService, OuterMap} from "./insights-service.service";
import {FormBuilder} from "@angular/forms";
import {MatDialog} from "@angular/material/dialog";
import {InsightsTransactionsDialogComponent} from "./insights-transactions-dialog.component";
import {MatSelectChange} from "@angular/material/select";
import {HttpClient} from "@angular/common/http";
import {ChartData, ChartType} from "chart.js";

@Component({
  selector: 'app-insights',
  template: `
    <body>

    <!--<br> <button routerLink="/menu">Back to menu</button><br> -->

    <div class="insights-container">
      <div>
        <div class="account-selector">
          <mat-form-field class="form">
            <mat-label>Select another account to see specific data</mat-label>
            <mat-select [(ngModel)]="account" (selectionChange)="onAccountSelected($event)">
              <mat-option *ngFor="let accountS of accounts" [value]="accountS">{{accountS}}</mat-option>
            </mat-select>
          </mat-form-field>
        </div>


        <br>
        <div class="date-range">
          <mat-form-field class="form">
            <mat-label>Enter a date range</mat-label>
            <mat-date-range-input [rangePicker]="picker">
              <input matStartDate placeholder="Start date" [(ngModel)]="startDate">
              <input matEndDate placeholder="End date" [(ngModel)]="endDate" (dateChange)="getSpendingsBetweenDates()">
            </mat-date-range-input>
            <mat-hint>MM/DD/YYYY – MM/DD/YYYY
              <br> *To display a chart, enter data range
            </mat-hint>
            <mat-datepicker-toggle matIconSuffix [for]="picker"></mat-datepicker-toggle>
            <mat-date-range-picker #picker></mat-date-range-picker>
          </mat-form-field>
        </div>

        <br>
        <div class="toogle-item">
          <mat-button-toggle-group class='btnToogle' name="fontStyle" aria-label="Font Style"
                                   [(ngModel)]="selectedValue" (click)="onToggleChange()">
            <mat-button-toggle value="euro">Euro</mat-button-toggle>
            <mat-button-toggle value="co2">CO2</mat-button-toggle>
          </mat-button-toggle-group>

        </div>




        <div *ngFor="let monthKey of sortedKeys">
          <div class="insights-date">{{ monthKey }}</div>
          <hr>
          <table class="table1">
            <tr *ngFor="let item of groupedData[monthKey]">
              <td>
                <button mat-raised-button
                        (click)="openInsightsTransactionsDialog(item.category, monthKey, username, account, startDate, endDate)">{{ item.category }}</button>
              </td>

              <td>
                <span *ngIf = "selectedValue === 'euro'" class="amount" >{{ item.value | currency: 'EUR' }}</span>
                <span *ngIf = "selectedValue === 'co2'" class="amount" >{{ item.value }}g</span>
              </td>


            </tr>
          </table>
        </div>

        <div class="chart"
             style="border-color: black; border-bottom-width: thick; display: flex; justify-content: center; align-items: center; width: 400px; height: 400px;"
             *ngIf="pieChartData.labels  && pieChartData.labels.length > 0">
          <canvas baseChart [data]="pieChartData" [labels]="pieChartData.labels" [type]="pieChartType"></canvas>
        </div>
      </div>
    </div>

    </body>

  `,
  styleUrls: ['./insights.scss']
})
export class InsightsComponent implements OnInit {

  iban: string | null = '';
  spendings: OuterMap = {};
  insights: Insights[] = [];
  startDate!: Date;
  endDate!: Date;
  selectedValue: string = 'euro';
  sortedKeys!: string[];

  username: string = '';
  account: string = '';

  //username:string = history.state.username;
  //account:string = history.state.account;
  insightsTransactions: any[] = [];

  accounts: any[] = []
  groupedData!: { [key: string]: Insights[] };


  public pieChartData: ChartData = {
    labels: [],
    datasets: [
      {
        data: [],
      },
    ],
  };
  public pieChartType: ChartType = 'pie';


  onToggleChange() {

    if ((this.selectedValue == 'euro') && (this.startDate == null)) {
      console.log('euro');
      this.insightsService.getSpendingsInEuro(this.account).subscribe(spendings => {
          this.spendings = spendings;
          this.insights = this.insightsService.transformSpendingsData(this.spendings);
          this.insightsService.sortInsightsByCategory(this.insights);
          this.insightsService.sortInsightsByMonth(this.insights);
          this.groupedData = this.insightsService.groupDataByMonth(this.insights);
          this.sortedKeys = this.insightsService.sortKeysToDisplayInsights(this.groupedData);
          console.log(this.sortedKeys);
          console.log('Spendings are ', this.insights);
          // console.log('Sorted are ' ,this.sorted);
        },
        (error) => {
          console.error('Error:', error);
        });

    } else if ((this.selectedValue == 'co2') && (this.startDate == null)) {
      console.log('co2');
      this.insightsService.getSpendingsInCO2(this.account).subscribe(spendings => {
          this.spendings = spendings;
          console.log(this.spendings)
          this.insights = this.insightsService.transformSpendingsData(this.spendings);
          this.insightsService.sortInsightsByCategory(this.insights);
          this.insightsService.sortInsightsByMonth(this.insights);
          this.groupedData = this.insightsService.groupDataByMonth(this.insights);
          this.sortedKeys = this.insightsService.sortKeysToDisplayInsights(this.groupedData);
          console.log('Spendings are ', this.insights);
          // console.log('Sorted are ' ,this.sorted);
        },
        (error) => {
          console.error('Error:', error);
        });

    } else if ((this.selectedValue == 'co2') && (this.startDate != null)) {
      console.log('co2');
      this.insightsService.getSpendingsBetweenDatesInCO2(this.account, this.startDate, this.endDate).subscribe(spendings => {
          this.spendings = spendings;
          this.insights = this.insightsService.transformSpendingsData(this.spendings);
          this.insightsService.sortInsightsByCategory(this.insights);
          this.insightsService.sortInsightsByMonth(this.insights);
          this.groupedData = this.insightsService.groupDataByMonth(this.insights);
          this.sortedKeys = this.insightsService.sortKeysToDisplayInsights(this.groupedData);

          const excludedLabels = ['Total[g]', 'Total[€]'];
          const filteredInsights = this.insights.filter(insight => !excludedLabels.includes(insight.category));

          const data = filteredInsights.map(insight => insight.value);
          const labels = filteredInsights.map(insight => insight.category);
          console.log('Spendings are ', this.insights);

          this.pieChartData = {
            labels: labels,
            datasets: [
              {
                data: data,
              },
            ],
          }
          // console.log('Sorted are ' ,this.sorted);
        },
        (error) => {
          console.error('Error:', error);
        });

    } else if ((this.selectedValue == 'euro') && (this.startDate != null)) {
      console.log('co2');
      this.insightsService.getSpendingsBetweenDatesInEuro(this.account, this.startDate, this.endDate).subscribe(spendings => {
          this.spendings = spendings;

          this.insights = this.insightsService.transformSpendingsData(this.spendings);
          this.insightsService.sortInsightsByCategory(this.insights);
          this.insightsService.sortInsightsByMonth(this.insights);
          this.groupedData = this.insightsService.groupDataByMonth(this.insights);
          this.sortedKeys = this.insightsService.sortKeysToDisplayInsights(this.groupedData);
          const excludedLabels = ['Total[g]', 'Total[€]'];
          const filteredInsights = this.insights.filter(insight => !excludedLabels.includes(insight.category));

          const data = filteredInsights.map(insight => insight.value);
          const labels = filteredInsights.map(insight => insight.category);
          console.log(this.insights);

          this.pieChartData = {
            labels: labels,
            datasets: [
              {
                data: data,
              },
            ],
          }


          // console.log('Sorted are ' ,this.sorted);
        },
        (error) => {
          console.error('Error:', error);
        });
    }

  }


  constructor(private route: ActivatedRoute,
              private insightsService: InsightsServiceService,
              private fb: FormBuilder,
              private dialog: MatDialog,
              private http: HttpClient) {
  }

  openInsightsTransactionsDialog(category: string, month: string, username: string, account: string, startDate: Date, endDate: Date) {
    console.log('AStart Date:', this.startDate);
    console.log('AEnd Date:', this.endDate);
    console.log('BStart Date:', startDate);
    console.log('BEnd Date:', endDate);
    this.insightsService.getSpecificInsightsTransactions(category, month, account, startDate, endDate).subscribe(data => {
      this.insightsTransactions = data;
      this.dialog.open(InsightsTransactionsDialogComponent, {
        data: this.insightsTransactions
      });
    });
    console.log("Insights are " + this.startDate);
    //this.insightsTransactions = [];

  }

  onAccountSelected(event: MatSelectChange) {
    const selectedAccount = event.value;
    this.onToggleChange();

  }


  getSpendingsBetweenDates() {
    console.log('Start Date:', this.startDate);
    console.log('End Date:', this.endDate);
    if (this.selectedValue == 'co2') {
      this.insightsService.getSpendingsBetweenDatesInCO2(this.account, this.startDate, this.endDate).subscribe(spendings => {
          this.spendings = spendings;
          this.insights = this.insightsService.transformSpendingsData(this.spendings);
          this.insightsService.sortInsightsByCategory(this.insights);
          this.insightsService.sortInsightsByMonth(this.insights);
          this.groupedData = this.insightsService.groupDataByMonth(this.insights);
          this.sortedKeys = this.insightsService.sortKeysToDisplayInsights(this.groupedData);
          console.log('Spendings are ', this.insights);
          // console.log('Sorted are ' ,this.sorted);
        },
        (error) => {
          console.error('Error:', error);
        });
    } else if (this.selectedValue == 'euro') {
      this.insightsService.getSpendingsBetweenDatesInEuro(this.account, this.startDate, this.endDate).subscribe(spendings => {
          this.spendings = spendings;
          this.insights = this.insightsService.transformSpendingsData(this.spendings);
          this.insightsService.sortInsightsByCategory(this.insights);
          this.insightsService.sortInsightsByMonth(this.insights);
          this.groupedData = this.insightsService.groupDataByMonth(this.insights);
          this.sortedKeys = this.insightsService.sortKeysToDisplayInsights(this.groupedData);
          console.log('Spendings are ', this.insights);
          // console.log('Sorted are ' ,this.sorted);
        },
        (error) => {
          console.error('Error:', error);
        });
    }
    this.onToggleChange()
  }

  ngOnInit() {

    let _account = sessionStorage.getItem("account");
    if (_account == null) throw new Error("user not logged in");
    this.account = _account;

    let _username = sessionStorage.getItem("username");
    if (_username == null) throw new Error("user not logged in");
    this.username = _username;

    // this.account = sessionStorage.getItem("account")

    this.iban = this.route.snapshot.paramMap.get('iban');
    this.http.get<any[]>('http://localhost:8080/accounts', {
      params: {
        customerName: this.username,
      }
    }).subscribe(accounts => {
      this.accounts = accounts;
    });
    this.onToggleChange();


  }

}
