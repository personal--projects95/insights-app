import {Component, OnInit} from '@angular/core';
import {TransactionServiceComponent} from "./transaction-service.component";
import {HttpClient} from "@angular/common/http";
import {MatSelectChange} from '@angular/material/select';
import * as IBAN from 'iban';

@Component({
  selector: 'app-send-money',
  template: `
    <body>
    <!-- <br> <button routerLink="/menu/{{account}}">Back to menu</button><br> -->
    <div class="send-money-container">
      <div>


        <mat-card class="example-card">
          <mat-card-header>
          </mat-card-header>
          <mat-card-content>
            <h3 class="text-size" >Your balance is {{balance | currency: "EUR"}}</h3>
            <mat-form-field>
              <mat-label>Sent from [iban]</mat-label>
              <mat-select [(ngModel)]="account" (selectionChange)="onAccountSelected($event)">
                <mat-option *ngFor="let accountS of accounts" [value]="accountS">{{ accountS }}</mat-option>
              </mat-select>
            </mat-form-field>
          </mat-card-content>
        </mat-card>

        <div class="formular">

          <mat-form-field class="form-size">
            <mat-label>Send to [name]</mat-label>
            <input matInput [(ngModel)]="receiverNameF">
          </mat-form-field>
          <br>
          <mat-form-field class="form-size">
            <mat-label>Send to [iban]</mat-label>
            <input matInput [(ngModel)]="receiverIBANF">
          </mat-form-field>
          <br>
          <mat-form-field class="form-size">
            <mat-label>Amount</mat-label>
            <input matInput type="number" [(ngModel)]="amountF">
          </mat-form-field>
          <br>
          <mat-form-field class="form-size">
            <mat-label>Description</mat-label>
            <input matInput [(ngModel)]="descript">
          </mat-form-field>

          <div class="send-form">
            <button mat-raised-button (click)="sendMoneyTest()">
              Send money
            </button>
          </div>
        </div>

        <div class="emptyFields">{{ emptyFields }}</div>
        <div class="noBalance">{{ noBalance }}</div>
        <div class="status">{{ status }}</div>
        <div class="IBANformat">{{ IBANformat }}</div>
      </div>
    </div>

    </body>



  `,
  styleUrls: ['./send-money.scss']
})
export class SendMoneyComponent implements OnInit {

  // username:string = history.state.username;
  // account:string = history.state.account;

  username: string = '';
  account: string = '';

  accounts: any[] = []

  receiverNameF: string = "";
  receiverIBANF: string = "";
  descript: string = "";
  amountF!: number;
  amountFinal: string = "";


  emptyFields: string = "";
  noBalance: string = "";
  IBANformat: string = "";
  status: string | null = "";
  balance: any

  constructor(
    private transactionService: TransactionServiceComponent,
    private http: HttpClient) {
  }

  resetFields() {
    this.descript = '';
    this.amountF = 0;
    this.receiverIBANF = '';
    this.receiverNameF = '';
  }

  resetStatuses(){
    this.emptyFields = '';
    this.noBalance = '';
    this.IBANformat = '';
  }

  sendMoney(receiverNameAccount: string, receiverIBAN: string, amount: string, description: string, username: string, account: string): void {
    console.log(this.balance, Number(amount))
    if (this.balance >= Number(amount)) {
      this.noBalance = ""
      if ((receiverIBAN) && (receiverNameAccount) && (amount) && (description)) {
        this.emptyFields = ""
        this.transactionService.addTransaction(receiverNameAccount, receiverIBAN, amount, description, account);
        //this.status = 'Transaction made successfully!'
        sessionStorage.setItem('status', 'Transaction made successfully!')
        ////////////////////////window.location.reload();
        //this.status =
        //console.log("success")
      } else {
        this.emptyFields = "One or more fields are empty!"
      }
    } else {
      this.noBalance = "Not enough balance!"
      this.emptyFields = ""

    }
  }

  ngOnInit() {
    let _account = sessionStorage.getItem("account");
    if (_account == null) throw new Error("user not logged in");
    this.account = _account;

    let _username = sessionStorage.getItem("username");
    if (_username == null) throw new Error("user not logged in");
    this.username = _username;

    this.transactionService.getBalance(this.account).subscribe(balance => this.balance = balance);
    //this.status = sessionStorage.getItem('status');
    this.http.get<any[]>('http://localhost:8080/accounts', {
      params: {
        customerName: this.username,
      }
    }).subscribe(accounts =>
      this.accounts = accounts
    );
    //sessionStorage.removeItem('status');
  }

  onAccountSelected(event: MatSelectChange) {
    const selectedAccount = event.value;
    this.transactionService.getBalance(selectedAccount).subscribe(balance => this.balance = balance)
  }

  //sendMoneyTest(receiverNameF: string, receiverIBANF: string, account: string, description: string, amountF: number) {
  sendMoneyTest() {
    let transaction = {
      receiverNameAccount: this.receiverNameF,
      receiverIBAN: this.receiverIBANF,
      senderIBAN: this.account,
      description: this.descript,
      amount: this.amountF
    }
   // console.log(transaction);
   // this.http.post<any[]>('http://localhost:8080/transactions/test', transaction, {}, ).subscribe(
    //  (response) => {},
    //  (response) => {});



    //console.log(receiverNameF, receiverIBANF, account, description, amountF);
   // console.log(amountF);

    // let IBAN = require('iban');

    if (typeof transaction.amount === "undefined") {
      const variab = 0;
      this.amountFinal = variab.toString();
    } else {
      this.amountFinal = transaction.amount.toString();
    }
    if (this.balance >= Number(this.amountFinal)) {
      this.noBalance = ""

      if ((transaction.receiverIBAN) && (transaction.receiverNameAccount) && (transaction.amount) && (transaction.description)) {
        this.emptyFields = ""

        if (IBAN.isValid(transaction.receiverIBAN)) {

          //this.transactionService.addTransaction(receiverNameF, receiverIBANF, this.amountFinal, description, username, account);


          this.http.post<any[]>('http://localhost:8080/transactions/test', transaction, {}, ).subscribe(
            (response) => {},
            (response) => {this.status = "Something went wrong! Try again"});

          //this.status = 'Transaction made successfully!'
          // sessionStorage.setItem('status', 'Transaction made successfully!')
          /////////// window.location.reload();
          this.resetFields();
          this.resetStatuses();
          this.status = "Transaction added successfully!"
          //this.status =
          //console.log("success")
        } else {
          this.IBANformat = "IBAN format is wrong!"
        }
      } else {
        this.emptyFields = "One or more fields are empty!"
      }
    } else {
      this.noBalance = "Not enough balance!"
      this.emptyFields = ""
    }

  }
}
