import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {TransactionServiceComponent} from './transaction-service.component';
import {MatDialog} from "@angular/material/dialog";
import {ChangeCategoryDialogComponent} from "./change-category-dialog.component";
import {MatSelectChange} from "@angular/material/select";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-transactions',
  template: `

    <!--<br> <button routerLink="/menu">Back to menu</button><br>-->
    <body>

    <div class="transactions-container">
      <div>



        <mat-card class="example-card">
          <mat-card-header>
          </mat-card-header>
          <mat-card-content>
            <h3 class="text-size" >Your balance is {{balance | currency: "EUR"}}</h3>
            <div class="account-selector">
              <mat-form-field class="form">
                <mat-label>Select another account to see specific data</mat-label>
                <mat-select [(ngModel)]="account" (selectionChange)="onAccountSelected($event)">
                  <mat-option *ngFor="let accountS of accounts" [value]="accountS">{{accountS}}</mat-option>
                </mat-select>
              </mat-form-field>
            </div>
          </mat-card-content>
        </mat-card>


        <!-- <ng-container *ngFor="let group of grTransactions | keyvalue"> -->
        <!--<br><p>{{ group.key | date:"d MMM yyyy" }}</p><hr>
        <mat-toolbar class="toolbar-row" >
          <mat-toolbar-row >
            {{ group.key | date:"d MMM yyyy" }}
          </mat-toolbar-row>
        </mat-toolbar>-->
        <ng-container *ngFor="let group of grTransactionsArray">
          <div class="transactions-date">
            {{ group.date | date:"d MMM yyyy" }}
            <hr>
          </div>
          <div class="center-accordion">
            <mat-accordion class="center-accordion">
              <!--  <ng-container *ngFor="let transaction of group.value"> -->
              <ng-container *ngFor="let transaction of group.transactions">
                <mat-expansion-panel>
                  <mat-expansion-panel-header>

                    <mat-panel-title>
                      <span
                        *ngIf="transaction.senderIBAN === account; else sender">{{ transaction.receiverNameAccount }}</span>
                      <ng-template #sender>{{ transaction.senderNameAccount }}</ng-template>
                    </mat-panel-title>

                    <mat-panel-description>
                      <span
                        *ngIf="transaction.senderIBAN === account; else positiveAmount">{{'- ' + (transaction.amount | currency: 'EUR')}}</span>
                      <ng-template #positiveAmount>{{'+ ' + (transaction.amount | currency: 'EUR')}}</ng-template>
                    </mat-panel-description>
                  </mat-expansion-panel-header>
                  <p><b>Execution date</b>
                    <br> {{transaction.date | date:"d MMM yyyy"}}
                  </p>
                  <hr>
                  <p>
                    <b><span *ngIf="transaction.senderIBAN === account; else senderTr">To account</span></b>
                    <b>
                      <ng-template #senderTr>From account</ng-template>
                    </b>
                    <br><span
                    *ngIf="transaction.senderIBAN === account; else senderT">{{transaction.receiverIBAN}}</span>
                    <ng-template #senderT>{{transaction.senderIBAN}}</ng-template>

                  </p>
                  <hr>
                  <p><b>Description</b>
                    <br>{{transaction.description}}
                  </p>
                  <hr>
                  <p><b>Category</b>
                    <br>
                    <button
                      (click)="openDialog(transaction.senderIBAN, transaction.receiverIBAN, transaction.amount, transaction.description, transaction.category)">{{transaction.category}}</button>
                  </p>
                </mat-expansion-panel>
              </ng-container>
            </mat-accordion>
            <br>
          </div>
        </ng-container>
      </div>


    </div>
    </body>




  `,


  styleUrls: ['./transactions.scss']
})
export class TransactionsComponent implements OnInit {

  iban: string | null = '';
  transactions: any[] = []
  category: string = ''
  balance: any

  username: string = '';
  //account:string ='';

  //username:string = history.state.username;
  //account:string = history.state.account;
  account: string = '';

  groupedTransactions: Map<string, any[]> = new Map();
  grTransactions: Map<string, any[]> = new Map();
  grTransactionKeys: string[] = [];
  grTransactionsArray: { date: string, transactions: any[] }[] = [];

  accounts: any[] = []


  constructor(
    private transactionService: TransactionServiceComponent,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private http: HttpClient) {
  }


  openDialog(senderIBAN: string, receiverIBAN: string, amount: number, description: string, currentCategory: string): void {
    const dialogRef = this.dialog.open(ChangeCategoryDialogComponent, {
      data: {
        currentCategory,
        allCategories: ['groceries', 'housing', 'others', 'restaurant', 'airTravel', 'utilities', 'groundTravel']
      }
    });
    dialogRef.afterClosed().subscribe(newCategory =>
      this.transactionService.changeCategory(senderIBAN, receiverIBAN, amount.toString(), description, newCategory));
  }

  ngOnInit() {
    let _account = sessionStorage.getItem("account");
    if (_account == null) throw new Error("user not logged in");
    this.account = _account;

    let _username = sessionStorage.getItem("username");
    if (_username == null) throw new Error("user not logged in");
    this.username = _username;

    this.grTransactionsArray = []
    this.grTransactions = new Map()
    //this.username = history.state.username;
    // this.account = history.state.account;
    //this.iban = this.route.snapshot.paramMap.get('iban');
    this.http.get<any[]>('http://localhost:8080/accounts', {
      params: {
        customerName: this.username,
      }
    }).subscribe(accounts => {
      this.accounts = accounts;
      //this.account = this.accounts[0]
      this.transactionService.getBalance(this.account).subscribe(balance => this.balance = balance);
    });

    console.log("aici");
    console.log(this.username, this.account);
    this.transactionService.getTransactions(this.account).subscribe(data => {
      this.transactions = data
      // console.log(this.transactions)
      this.groupTransactionsByDate();
      // console.log(this.groupedTransactions)
      this.grTransactions = this.groupedTransactions;
      //this.grTransactionKeys = Array.from(this.groupedTransactions.keys());
      this.groupedTransactions.forEach((transactionsForDate, dateKey) => {
        this.grTransactionsArray.push({date: dateKey, transactions: transactionsForDate});
      });
      console.log("gr")
      console.log(this.grTransactionsArray)
    });


  }

  onAccountSelected(event: MatSelectChange) {
    const selectedAccount = event.value;
    //this.account = selectedAccount;
    history.pushState({account: selectedAccount, username: this.username}, '', null);
    console.log(history.state.account)
    this.grTransactionsArray = []
    //this.grTransactions = new Map()
    //this.account = selectedAccount;
    this.transactionService.getBalance(selectedAccount).subscribe(balance => this.balance = balance);
    this.transactionService.getTransactions(selectedAccount).subscribe(data => {
      console.log(this.transactions)
      this.transactions = [];
      this.groupedTransactions = new Map();
      //this.grTransactionsArray = []
      // this.grTransactions = new Map()
      this.transactions = data;
      //console.log(this.transactions)
      this.groupTransactionsByDate();
      this.groupedTransactions.forEach((transactionsForDate, dateKey) => {
        this.grTransactionsArray.push({date: dateKey, transactions: transactionsForDate});
      });


      sessionStorage.setItem('account', selectedAccount);
      console.log(this.account)
      //window.location.reload();
      //console.log(this.groupedTransactions)
    });

  }

  returnIBAN(): string {
    return <string>this.iban;
  }

  private groupTransactionsByDate() {
    this.transactions.forEach(transaction => {
      const dateKey = new Date(transaction.date).toISOString().split('T')[0];
      // Get the array associated with dateKey, or create an empty array if it doesn't exist
      const transactionsForDate = this.groupedTransactions.get(dateKey) || [];
      transactionsForDate.push(transaction);
      this.groupedTransactions.set(dateKey, transactionsForDate);
      //console.log(dateKey)
    });
    // console.log("testnumerge")
  }


  protected readonly JSON = JSON;
}
