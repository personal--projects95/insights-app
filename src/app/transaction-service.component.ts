import {Component, Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable} from "rxjs";

@Component({
  selector: 'app-transaction-service',
  template: `
    <p>
      transaction-service works!
    </p>
  `,
  styles: []
})
@Injectable({
  providedIn: 'root'
})
export class TransactionServiceComponent {

  constructor(private http: HttpClient) {
    /* this.http.get('http://localhost:8080/transactions', {
       params: {
         IBAN: 'NL02BOG3344556677',
       },
       observe: 'response'
     })
       .toPromise()
       .then(response => {
         console.log(response);
       })
       .catch(console.log);*/

  }

  getTransactions(iban: string) {
    console.log(iban)
    return this.http.get<any[]>('http://localhost:8080/transactions', {
      params: {
        IBAN: iban,
      }
    });
  }

  getBalance(iban: string) {
    return this.http.get<any[]>('http://localhost:8080/transactions/balance', {
      params: {
        IBAN: iban,
      }
    })
  }

  changeCategory(senderIBAN: string, receiverIBAN: string, amount: string, description: string, newCategory: string) {
    //TODO sent to BE
    this.http.put<string>('http://localhost:8080/transactions/update', null,
      {
        params: {
          receiverIBAN: receiverIBAN,
          amount: amount,
          senderIBAN: senderIBAN,
          description: description,
          category: newCategory,
        }
      }
    ).subscribe((response) => {
      console.log(response)
    });

    window.location.reload();
  }

  //this.transactions[id].category = newCategory;


  //addTransaction(receiverNameAccount: string ,receiverIBAN: string, amount: string, description: string, username: string, account: string){
  addTransaction(receiverNameAccount: string, receiverIBAN: string, amount: string, description: string, account: string): Observable<any> {

    //TODO Send to server
    //console.log(receiverIBAN, username, account)

    const params = new HttpParams()
      .set('receiverNameAccount', receiverNameAccount)
      .set('receiverIBAN', receiverIBAN)
      .set('amount', amount)
      .set('description', description)
      .set('senderIBAN', account);

    return this.http.post<any[]>('http://localhost:8080/transactions', null, {
      params: params,
    });

    /* return this.http.post<any[]>('http://localhost:8080/transactions', null,
       {
         params: {
           receiverNameAccount: receiverNameAccount,
           receiverIBAN: receiverIBAN,
           amount: amount,
           description: description,
           //username: username,
           senderIBAN: account
         }
       }
     ).subscribe((response) => {console.log(response)});*/

  }

}
