import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-menu',
  template: `


    <h4>
      What would you like to do today?
    </h4>
    <p>Your current account is {{account}}</p>
    <button (click)="goToInsights()">Insights</button>
    <br> <button (click)="goToTransactions()">Transactions</button>
    <br> <button (click)="goToAccountSelector()">Select another account</button>
    <br> <button (click)="goToSendMoney()">Send money</button>
    <br> <button routerLink="/log-out">Log out</button>


  `,
  styles: []
})
export class MenuComponent implements OnInit {
  selectedValue: any;
  selectedAccount = '';
  iban: string | null = '';
  username: string | null = '';
  account: string | null = '';
  //account:string = history.state.account;

  //username:string = history.state.username;
  //account:string = history.state.account;


  //@Input()
  //selectedAccount:string = "";

  constructor(private route: ActivatedRoute, private router: Router, private http: HttpClient) {
  }


  ngOnInit() {
    //this.route.params.subscribe(params => {this.selectedAccount = params['selectedAccount']});
    //this.iban = this.route.snapshot.paramMap.get('iban');

    sessionStorage.removeItem('status');
    this.account = sessionStorage.getItem('account');
    this.username = sessionStorage.getItem('username');
    console.log(this.username, this.account);


  }

  goToTransactions() {
    this.router.navigate(['/transactions'],
      {
        state: {
          username: this.username,
          account: sessionStorage.getItem('account')
        }
      });
  }

  goToInsights() {
    this.router.navigate(['/insights'],
      {
        state: {
          username: this.username,
          account: this.account
        }
      });
  }

  goToSendMoney() {
    this.router.navigate(['/send-money'],
      {
        state: {
          username: this.username,
          account: this.account
        }
      });
  }

  goToAccountSelector() {
    this.router.navigate(
      ["/account-selector/"],
      {
        state: {
          username: this.username,
        }
      }
    );
  }


  do() {
    console.log(this.iban);
  }

  mapIbanToIt() {
    //this.route.navigate(['/menu', this.selectedAccount]);

  }
}
