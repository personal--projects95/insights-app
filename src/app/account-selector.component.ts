import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-account-selector',
  template: `

    <div class="account">
      <label for="account">First, you need to select one account</label>

      <select class="selector" name="account" id="account" #account>
        <option *ngFor="let account of accountsss" [value]="account">{{account}}</option>
      </select>
      <div class="btn-div">
        <button type="submit" (click)="selectAccount(account.value)" class="submit-btn">
          Submit
        </button>
      </div>
    </div>

  `,
  styles: [
    '.selector {\n' +
    '  margin-top: 10px;\n' +
    'height: 30px;\n' +
    '}' +
    '.account {\n' +
    '  display: flex;\n' +
    '  align-items: center;\n' +
    '  justify-content: center;\n' +
    '  flex-direction: column;\n' +
    '  height: 98%;\n' +
    'size: 50px;' +
    '}' +
    '.btn-div {\n' +
    'margin-top: 10px;' +
    '  display: flex;\n' +
    '  align-items: center;\n' +
    '  justify-content: center;\n' +
    '}\n' +
    '\n' +
    '.btn-div .submit-btn {\n' +
    '  width: 15rem;\n' +
    '  padding: 6px;\n' +
    '  font-size: 14px;\n' +
    '  background: orange;\n' +
    '  outline: none;\n' +
    '  border: none;\n' +
    '  border-radius: 4px;\n' +
    '  color: white;\n' +
    '  font-weight: 700;\n' +
    '  cursor: pointer;\n' +
    '  letter-spacing: 1px;\n' +
    '}'
  ]
})
export class AccountSelectorComponent implements OnInit {


  selectedAccount: any;
  selectedValue: any;
  username: string = history.state.username;

  accounts = [
    'NL03BOG9988776655',
    'NL02BOG3344556677',
    'NL01BOG1122334455',
    'NL06BOG1234567890'
  ];
  accountsss: any[] = []


  selectAccount(account: string) {
    //console.log(account);
    //window.location.href = "/menu";
    // this.selectedValue = account;
    this.selectedAccount = account;
    //sessionStorage.setItem('account', this.selectedAccount);
    this.router.navigate(['/menu', this.selectedAccount],
      {
        state: {
          username: this.username,
          account: this.selectedAccount
        }
      });
  }


  constructor(private router: Router, private http: HttpClient) {
  }

  ngOnInit(): void {
    console.log(this.username);
    this.http.get<any[]>('http://localhost:8080/accounts', {
      params: {
        customerName: this.username,
      }
    }).subscribe(accounts =>
      this.accountsss = accounts
    );
  }

}
