import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

interface InnerMap {
  [key: string]: number;
}

export interface OuterMap {
  [key: string]: InnerMap;
}

export interface Insights {
  month: string;
  category: string;
  value: number;
}

@Injectable({
  providedIn: 'root'
})
export class InsightsServiceService {

  spendings: OuterMap = {};
  insights: Insights[] = [];

  constructor(private http: HttpClient) {
  }

  getSpendingsInCO2(iban: string) {
    //console.log(iban)
    return this.http.get<OuterMap>('http://localhost:8080/transactions/spendings/CO2', {
      params: {
        IBAN: iban,
      }
    })
  }

  getFinalSpendingsInCO2(iban: string) {
    this.getSpendingsInCO2(iban).subscribe(spendings => {
      this.spendings = spendings;
      this.insights = this.transformSpendingsData(this.spendings);
      this.sortInsightsByCategory(this.insights);
      this.sortInsightsByMonth(this.insights);
    });
    return this.insights;
  }

  getFinalSpendingsInEuro(iban: string) {
    this.getSpendingsInCO2(iban).subscribe(spendings => {
      this.spendings = spendings;
      this.insights = this.transformSpendingsData(this.spendings);
      this.sortInsightsByCategory(this.insights);
      this.sortInsightsByMonth(this.insights);
    });
    return this.insights;
  }

  getFinalSpendingsBetweenDatesInEuro(iban: string, startDate: Date, endDate: Date) {
    this.getSpendingsInCO2(iban).subscribe(spendings => {
      this.spendings = spendings;
      this.insights = this.transformSpendingsData(this.spendings);
      this.sortInsightsByCategory(this.insights);
      this.sortInsightsByMonth(this.insights);
    });
    return this.insights;
  }

  getFinalSpendingsBetweenDatesInCO2(iban: string, startDate: Date, endDate: Date) {
    this.getSpendingsInCO2(iban).subscribe(spendings => {
      this.spendings = spendings;
      this.insights = this.transformSpendingsData(this.spendings);
      this.sortInsightsByCategory(this.insights);
      this.sortInsightsByMonth(this.insights);
    });
    return this.insights;
  }

  getSpecificInsightsTransactions(category: string, month: string, account: string, startDate: Date, endDate: Date) {
    return this.http.get<any[]>('http://localhost:8080/transactions/insights', {
      params: {
        IBAN: account,
        category: category,
        month: month,
        startDate: startDate !== undefined && startDate !== null ? startDate.toLocaleDateString('en-GB') : 'null',
        endDate: endDate !== undefined && endDate !== null ? endDate.toLocaleDateString('en-GB') : 'null'
      }
    });

  }

  url = 'http://localhost:8080';
 //TODO replace url
  getSpendingsInEuro(iban: string) {
    //console.log(iban)
    return this.http.get<OuterMap>(this.url + '/transactions/spendings/euro', {
      params: {
        IBAN: iban,
      }
    })
  }

  getSpendingsBetweenDatesInCO2(iban: string, startDate: Date, endDate: Date) {
    //console.log(iban)
    return this.http.get<OuterMap>('http://localhost:8080/transactions/spendings/CO2/dates', {
      params: {
        IBAN: iban,
        startDate: startDate.toLocaleDateString('en-GB'),
        endDate: endDate.toLocaleDateString('en-GB')
      }
    })
  }

  getSpendingsBetweenDatesInEuro(iban: string, startDate: Date, endDate: Date) {
    //console.log(iban)
    return this.http.get<OuterMap>('http://localhost:8080/transactions/spendings/euro/dates', {
      params: {
        IBAN: iban,
        startDate: startDate.toLocaleDateString('en-GB'),
        endDate: endDate.toLocaleDateString('en-GB')

      }
    })
  }

  transformSpendingsData(spendingsData: any): Insights[] {
    const insights: Insights[] = [];
    for (const month in spendingsData) {
      for (const category in spendingsData[month]) {
        const insight: Insights = {
          month: month,
          category: category,
          value: spendingsData[month][category],
        };
        insights.push(insight);
      }
    }
    return insights;
  }

  groupDataByMonth(data: Insights[]): { [key: string]: Insights[] } {
    const groupedData: { [key: string]: Insights[] } = {};

    data.forEach((item) => {
      const month = item.month;
      if (!groupedData[month]) {
        groupedData[month] = [];
      }
      groupedData[month].push(item);
    });

    return groupedData;
  }

  sortKeysToDisplayInsights(groupedData: { [key: string]: Insights[] }){
    const sortedKeys = Object.keys(groupedData).sort((a, b) => {
      // Convert the keys (which are strings in 'YYYY-MONTH' format) to Date objects for comparison
      const dateA = new Date(a);
      const dateB = new Date(b);

      // Compare the Date objects in descending order
      return dateB.getTime() - dateA.getTime();
    });

    return sortedKeys
}

  sortInsightsByMonth(insights: Insights[]): Insights[] {
    function monthComparator(a: Insights, b: Insights): number {

      const aParts = a.month.split('-');
      const bParts = b.month.split('-');

      const aYear = aParts.length > 1 ? Number(aParts[0]) : new Date().getFullYear();
      const bYear = bParts.length > 1 ? Number(bParts[0]) : new Date().getFullYear();

      const aMonth = convertMonthToNumber(aParts[aParts.length - 1]);
      const bMonth = convertMonthToNumber(bParts[bParts.length - 1]);

      if (aYear !== bYear) {
        return aYear - bYear;
      }

      // If years are the same, compare months
      return aMonth - bMonth;
    }

    function convertMonthToNumber(monthName: string): number {

      const monthMapping: { [monthName: string]: number } = {
        'JANUARY': 12,
        'FEBRUARY': 11,
        'MARCH': 10,
        'APRIL': 9,
        'MAY': 8,
        'JUNE': 7,
        'JULY': 6,
        'AUGUST': 5,
        'SEPTEMBER': 4,
        'OCTOBER': 3,
        'NOVEMBER': 2,
        'DECEMBER': 1
      };

      return monthMapping[monthName];
    }


    return insights.sort(monthComparator);
  }

  sortInsightsByCategory(insights: Insights[]): Insights[] {
    // Custom comparator function to sort the categories based on categoryOrder
    const categoryOrder = [
      'Total[g]', 'Total[€]', 'airTravel', 'groundTravel', 'groceries', 'housing', 'others',
      'restaurant', 'utilities'
    ];

    function categoryComparator(a: Insights, b: Insights): number {
      const aCategoryIndex = categoryOrder.indexOf(a.category);
      const bCategoryIndex = categoryOrder.indexOf(b.category);

      return aCategoryIndex - bCategoryIndex;
    }

    // Sort the insights array based on the specified category order
    return insights.sort(categoryComparator);
  }

}
