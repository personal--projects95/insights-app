import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-insights-transactions-dialog',
  template: `
    <table mat-table [dataSource]="dataSource" class="mat-elevation-z8">

      <!-- Date Column -->
      <ng-container matColumnDef="date">
        <th mat-header-cell *matHeaderCellDef>Date</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.date | date:"d MMM yyy"}}</td>
      </ng-container>

      <ng-container matColumnDef="senderIBAN">
        <th mat-header-cell *matHeaderCellDef>From</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.senderIBAN }}</td>
      </ng-container>

      <!-- receiverIBAN Column -->
      <ng-container matColumnDef="receiverIBAN">
        <th mat-header-cell *matHeaderCellDef>To</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.receiverIBAN }}</td>
      </ng-container>

      <!-- receiverNameAccount Column -->
      <ng-container matColumnDef="receiverNameAccount">
        <th mat-header-cell *matHeaderCellDef>receiverName</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.receiverNameAccount }}</td>
      </ng-container>

      <!-- description Column -->
      <ng-container matColumnDef="description">
        <th mat-header-cell *matHeaderCellDef>Description</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.description }}</td>
      </ng-container>

      <!-- amount Column -->
      <ng-container matColumnDef="amount">
        <th mat-header-cell *matHeaderCellDef>Amount</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.amount | currency: 'EUR' }}</td>
      </ng-container>

      <ng-container matColumnDef="co2">
        <th mat-header-cell *matHeaderCellDef>CO2</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.co2 + 'g'}}</td>
      </ng-container>

      <ng-container matColumnDef="category">
        <th mat-header-cell *matHeaderCellDef>Category</th>
        <td mat-cell *matCellDef="let transaction">{{ transaction.category }}</td>
      </ng-container>


      <!-- Table Rows -->
      <tr mat-header-row *matHeaderRowDef="displayedColumns"></tr>
      <tr mat-row *matRowDef="let row; columns: displayedColumns;"></tr>

    </table>

  `,
  styles: []
})
export class InsightsTransactionsDialogComponent {

  //@Input() dataSource!: any[];
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['date', 'senderIBAN', 'receiverIBAN', 'receiverNameAccount', 'description', 'amount', 'co2', 'category'];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    // dataSource will be passed from the parent component when opening the dialog
    this.dataSource = new MatTableDataSource<any>(data);
  }

}
