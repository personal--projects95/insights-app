import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";

@Component({
  selector: 'app-change-category-dialog',
  template: `
    <h1 mat-dialog-title> Change category </h1>
    <div mat-dialog-content>
      <mat-form-field>
        <mat-label>Category</mat-label>
        <mat-select [(value)]="selectedCategory">
          <mat-option *ngFor="let category of data.allCategories" [value]="category">{{category}}</mat-option>
        </mat-select>
      </mat-form-field>

    </div>
    <div mat-dialog-actions>
      <button mat-button [mat-dialog-close]="selectedCategory">Save</button>
    </div>

  `,
  styles: []
})
export class ChangeCategoryDialogComponent {

  selectedCategory: string = '';

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    this.selectedCategory = data.currentCategory;

  }
}

export interface CategoryDialogData {
  currentCategory: string;
  allCategories: string[];

}
