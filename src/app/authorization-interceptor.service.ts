import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthorizationInterceptorService implements HttpInterceptor {

  getCsrfToken(): string | null {
    let csrfToken = null;
    document.cookie.split(';').forEach((cookie) => {
      const [name, value] = cookie.split('=').map(c => c.trim());
      console.log(name);
      if (name === 'XSRF-TOKEN') {
        csrfToken = value;
      }
    });
    return csrfToken;
  }

  constructor(private router: Router) {
  }

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    console.log("interceptor here");
    let username = sessionStorage.getItem("username");
    let password = sessionStorage.getItem("password");

    if (username != null) {
      //let X_XSRF_TOKEN = "X-XSRF-TOKEN"
      console.log(username, password);
      let header = 'Basic ' + btoa(username + ':' + password);
      console.log(this.getCsrfToken())

      let headers = new HttpHeaders()
        .set('Authorization', header);
      /*let csrfToken = this.getCsrfToken();
      if (csrfToken != null){
        headers = headers.set('X-XSRF-TOKEN',  csrfToken);
      }*/
      request = request.clone({
        headers: headers
      });
      console.log(request.url)

    } else {

      this.router.navigate(
        ["/log-in/"],
      );
    }

    /*if (this.getCsrfToken() != null) {
      X_XSRF_TOKEN = this.getCsrfToken();
      request = request.clone({
        headers: new HttpHeaders()
          .set('X-XSRF-TOKEN',  X_XSRF_TOKEN)
      });
    }*/

    return next.handle(request)
    /*
    .pipe(
    catchError((err) => {
      if (err.status === 401) {
      }
      const error = err.error.message || err.statusText;
      console.log(error)
      return throwError(error);
    }))
  */
  }
}
