import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {SignInComponent} from './sign-in.component';
import {TransactionsComponent} from './transactions.component';
import {AccountSelectorComponent} from './account-selector.component';
import {InsightsComponent} from './insights.component';
import {RouterModule} from "@angular/router";
import {MenuComponent} from './menu.component';
import {SendMoneyComponent} from './send-money.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonModule} from '@angular/material/button';
import {ToastrModule} from "ngx-toastr";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TransactionServiceComponent} from './transaction-service.component';
import {ChangeCategoryDialogComponent} from './change-category-dialog.component';
import {MatInputModule} from "@angular/material/input";
import {MatSelectModule} from "@angular/material/select";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatDialogModule} from "@angular/material/dialog";
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatNativeDateModule} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatButtonToggleModule} from "@angular/material/button-toggle";
import {InsightsTransactionsDialogComponent} from './insights-transactions-dialog.component';
import {MatTableModule} from '@angular/material/table';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatToolbarModule} from '@angular/material/toolbar';
import {NgChartsModule} from 'ng2-charts';
import {NavigationComponent} from './navigation/navigation.component';
import {AuthorizationInterceptorService} from "./authorization-interceptor.service";
import {MatCardModule} from "@angular/material/card";

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    TransactionsComponent,
    AccountSelectorComponent,
    InsightsComponent,
    MenuComponent,
    SendMoneyComponent,
    TransactionServiceComponent,
    ChangeCategoryDialogComponent,
    InsightsTransactionsDialogComponent,
    NavigationComponent,
  ],
    imports: [
        BrowserModule,
        NgChartsModule,
        MatTableModule,
        BrowserAnimationsModule,
        MatDialogModule,
        MatButtonModule,
        ReactiveFormsModule,
        MatInputModule,
        MatSelectModule,
        HttpClientModule,
        MatSlideToggleModule,
        MatFormFieldModule,
        AppRoutingModule,
        MatDatepickerModule,
        MatToolbarModule,
        MatNativeDateModule,
        RouterModule.forRoot([
            {path: 'sign-in', component: SignInComponent},
            //{path: 'account-selector/:username', component: AccountSelectorComponent},
            {path: 'account-selector', component: AccountSelectorComponent},
            //{path: 'transactions', component: TransactionsComponent},
            //{path: 'insights', component: InsightsComponent},
            //{path: 'menu', component: MenuComponent},
            {path: 'send-money', component: SendMoneyComponent},
            {path: 'log-out', redirectTo: 'sign-in', pathMatch: 'full'},
            // {path: 'menu/:iban', component: MenuComponent},
            {path: 'menu', component: MenuComponent},
            //{path: 'insights/:iban', component: InsightsComponent},
            {path: 'insights', component: InsightsComponent},
            {path: 'transactions', component: TransactionsComponent},
            //{path: 'transactions/:iban', component: TransactionsComponent},
            {path: '', redirectTo: 'sign-in', pathMatch: 'full'},
            {path: '**', redirectTo: 'sign-in', pathMatch: 'full'}
        ]),
        MatButtonModule,
        MatMenuModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
        FormsModule,
        MatButtonToggleModule,
        MatExpansionModule,
        NgChartsModule,
        MatCardModule,
    ],
  exports: [RouterModule],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthorizationInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule {


}
