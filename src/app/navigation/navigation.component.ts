import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  showMenu: boolean = false;
  username: string | null = '';
  account: string | null = '';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.account = sessionStorage.getItem('account');
    this.username = sessionStorage.getItem('username');
    console.log("newwwwww");
    console.log(this.username, this.account);

    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.showMenu = this.shouldShowMenu(event.url);
        console.log(this.showMenu)
        this.account = sessionStorage.getItem('account');
        this.username = sessionStorage.getItem('username');
        console.log("newwwwww");
        console.log(this.username, this.account);
      }
    });

  }

  private shouldShowMenu(url: string): boolean {
    const routesToHideMenu = ['/sign-in']; // Add routes as needed
    return !routesToHideMenu.includes(url);
  }

  goToTransactions() {
    /* this.router.navigate(['/transactions'],
       {
         state: {
           username: this.username,
           account: this.account
         }
       });*/

  }

  goToInsights() {
    /*this.router.navigate(['/insights'],
      {
        state: {
          username: this.username,
          account: this.account
        }
      });*/
  }

  goToSendMoney() {
    /* this.router.navigate(['/send-money'],
       {
         state: {
           username: this.username,
           account: this.account
         }
       });*/
  }

  goToLogOut() {
    history.pushState({account: "", username: ""}, '', null);
    sessionStorage.removeItem('account');
    sessionStorage.removeItem('username');
    this.router.navigate(['/sign-in']);
    //sessionStorage.removeItem('status');
    //sessionStorage.removeItem('account');
    //sessionStorage.removeItem('username');

  }

}
