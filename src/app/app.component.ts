import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'insightsApp';

  constructor(private router: Router) {
  }


  ngOnInit(): void {
    console.log("app component here")
    let username = sessionStorage.getItem("username");
    if (username == null) {
      this.router.navigate(
        ["/sign-in/"],
      );
    }
  }
}
