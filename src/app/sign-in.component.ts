import {Component, OnInit} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";

@Component({
  selector: 'app-sign-in',
  template: `

    <div class="align-center">
      <h2>Please, sign-in</h2>
      <form #myForm="ngForm" (ngSubmit)="onSubmit(myForm.value)">
        <input
          placeholder="Username"
          type="text"
          name="username"
          #username
        />
        <input
          placeholder="Password"
          type="password"
          name="password"
          #password
        />
        <div class="btn-div">
          <button type="submit" (click)="isAuth(username.value, password.value)" [disabled]="!myForm.valid"
                  class="login-btn">
            Login
          </button>
        </div>
      </form>
      <div class="notLogged"> {{value}}</div>
      <hr>
      <div class="newCustomer">If you dont have an account, register to the nearest agency!</div>
    </div>


  `,
  styles: [
    'hr {\n' +
    '  width: 400px;\n' +
    '  margin-left: 50;\n' +
    ' color: grey;' +
    '}' +
    '.newCustomer {font-style: italic;}' +
    '.notLogged {color: red;}' +
    '.align-center {\n' +
    '  display: flex;\n' +
    '  align-items: center;\n' +
    '  justify-content: center;\n' +
    '  flex-direction: column;\n' +
    '  height: 98%;\n' +
    '}\n' +
    '\n' +
    'form {\n' +
    '  display: flex;\n' +
    '  flex-direction: column;\n' +
    '}\n' +
    '\n' +
    'input {\n' +
    '  margin: 12px;\n' +
    '  padding: 8px;\n' +
    '  width: 14rem;\n' +
    '}\n' +
    '\n' +
    'input[type="password" i] {\n' +
    '  margin-top: 6px;\n' +
    '}\n' +
    '\n' +
    '.btn-div {\n' +
    '  display: flex;\n' +
    '  align-items: center;\n' +
    '  justify-content: center;\n' +
    '}\n' +
    '\n' +
    '.btn-div .login-btn {\n' +
    '  width: 15rem;\n' +
    '  padding: 6px;\n' +
    '  font-size: 14px;\n' +
    '  background: orange;\n' +
    '  outline: none;\n' +
    '  border: none;\n' +
    '  border-radius: 4px;\n' +
    '  color: white;\n' +
    '  font-weight: 700;\n' +
    '  cursor: pointer;\n' +
    '  letter-spacing: 1px;\n' +
    '}'

  ]
})
export class SignInComponent implements OnInit {

  isLogged = false;
  value: string = "";
  //username: string =''
  account: string = '';


  accounts: any[] = []

  constructor(private toastr: ToastrService, private http: HttpClient, private router: Router) {
  }

  onSubmit(user: string) {
    this.toastr.show("User logged in successfully");
    console.log(user);
  }

  ngOnInit() {
    this.isLogged = false;
  }

  isAuth(username: string, password: string) {
    console.log(username)
    sessionStorage.setItem('username', username);
    sessionStorage.setItem('password', password);
    let header = 'Basic ' + btoa(username + ':' + password);

    console.log(header)
    // if ((username == "admin") && (password == "admin")){
    // this.http.get<boolean>('http://localhost:8080/login', {
    // params: {
    //  username: username,
    //  password: password
    // },
    // headers: new HttpHeaders()
    //  .set('Authorization',  header)
    // }).subscribe(isValid => {
    // console.log(isValid)
    /// if (isValid) {
    //this.isLogged = true;
    sessionStorage.setItem('username', username);

    this.http.get<any[]>('http://localhost:8080/accounts', {
      params: {
        customerName: username,
      },
      // headers: new HttpHeaders()
      //   .set('Authorization',  header)
    }).subscribe(accounts => {
      this.accounts = accounts;
      this.account = this.accounts[0];
      sessionStorage.setItem('account', this.account);
      console.log("accccccccc" + sessionStorage.getItem("account"))
      this.router.navigate(['/transactions'],
        {
          state: {
            username: username,
            account: this.account,
            password: password
          }
        });
      // });

      // window.location.href = "/account-selector";
      /* this.router.navigate(
         ["/account-selector/"],
         {
           state: {
             username: username,
           }
         }
       );*/
      //this.router.navigate(['/menu']);


      // this.username = username;
      // this.router.navigate(['/account-selector', username]);
      /// return true;

      /// }
      ///this.isLogged = false;
      ///this.value = "Wrong credentials. Please, try again!!!!";
      /// return false;
    }, (error) => {
      this.value = "Wrong credentials. Please, try again!";
    });

  }

  //returnUsername(): string {
  //  return this.username;
  //}

}
